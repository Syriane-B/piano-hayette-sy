function removePlayed(event) {
    event.currentTarget.classList.remove('played');
}
//fonction qui récupère la balise audio qui a pour data key la même valeur que l'envent.keyCode
function playsound(event) {
    let audioKey = document.querySelector(`audio[data-key='${event.keyCode}']`);
    let divKey = document.querySelector(`div[data-key='${event.keyCode}']`);
    if (audioKey !== null) {
        divKey.classList.add('played');
        audioKey.currentTime = "0";
        audioKey.play();
    }
}

//appel de la fonction en event listner de touches
window.addEventListener('keydown', playsound);

let divTouches = document.querySelectorAll('#container > div');
for (const div of divTouches) {
    div.addEventListener('click', function () {
        let dataKeyValue = div.getAttribute("data-key");
        document.querySelector(`audio[data-key='${dataKeyValue}']`).play();
        div.classList.add('played');
    })
    div.addEventListener('transitionend', removePlayed);
}