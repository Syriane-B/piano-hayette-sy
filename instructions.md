## Soundboard

L'objectif est de creer une table de sons, jouable avec les touches du clavier, comme un piano.

Merci à https://github.com/wesbos/JavaScript30/tree/master/01%20-%20JavaScript%20Drum%20Kit pour les fichiers audio et les idées

1. Creer un nouveau dossier avec dedans : 1 fichier HTML, 1 fichier JS et les relier ensemble
2. Copier, dans ce dossier, le dossier contenant les sons
3. Dans le fichier HTML, ajouter autant de balise `<audio>` qu'il y a de sons.
La balise `<audio>` est une balise HTML5, avec un attribut `src` correspondant au son voulu. Il faut donc mettre le bon `src` dans chacune des balise
4. Ajouter autant de balise `<div>` qu'il y a de balise `<audio>`.
Celles-ci vont permettre de visualiser les touches du clavier disponible avec un son
5. Dans les `<div>`, ajouter deux attributs :  
   * Une classe `class="key"`
   * Un attribut `data-key="<code>"`. Remplacer `<code>` par le `keyCode` Javascript correspondant à la touche du clavier choisi.  
   Retrouver la liste des `keyCode` ici : https://keycode.info/  
   Exemple pour la touche 'G' : `data-key="71"`
6. Dans les `<audio>`, ajouter aussi un attribut `data-key="<code>"` avec le code voulu
7. Dans le fichier JS, creer une nouvelle `function playSound(event)`  
   Dans cette fonction, la variable `event` correspond à l'evenement quand une touche du clavier est pressée.
On peut recuperer le `keyCode` correspondant en faisant `event.keyCode`
8. Dans la fonction `playSound(event)`, il faut :  
   * Récuperer la bonne balise `<audio>` en fonction du `keyCode`. Pour ça on peut utiliser un `querySelector` comme en CSS.
   * Récuprer la bonne balise `<div>` en fonction du `keyCode`
   * Si la balise `<audio>` est trouvée, alors on joue le son. Voir `currentTime = 0` et `play()`
9. Ajouter un écouteur d'evenement (`addEventListener`) sur l'evenement `keydown`.  
   Il faut mettre cet écouteur sur `window`. L'écouteur appelle la fonction `playSound`

Vous devriez avoir une table de sons fonctionnelle ! Appuyez sur les touches du clavier pour tester.

Bonus : 
1. Donner du style à tout ça. Peut etre un style de piano ? Un soundboard ? Autre chose ?
2. Ajouter la possiblité de pouvoir jouer les sons au `click` avec la souris
3. Faire changer le style des touches quand je les tape au clavier :  
   * Creer une classe CSS avec le style voulu quand un son est joué.  
   Cette classe doit etre lier avec une `transition` sur l'element parent.
   * Dans la fonction `playSound(event)`, ajouter le code qui permet d'ajouter cette classe CSS
   * Ajouter une nouvelle `function removePlaying(event)` qui enlève la classe CSS sur la `currentTarget` de l'evenement
   * Ajouter un écouteur d'evenement sur chaque `.key`, sur l'evenement `transitionend` et qui fait appelle à la fonction `removePlaying`
